package com.unlimited.demo.rest.spring.configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

/**
 * @author Iulian Dumitru
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class ServiceConfigurationTest {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceConfigurationTest.class);

    @Value("${plugins.path}")
    private String pluginsDirPath;

    @Test
    public void testProp() throws Exception {

        assertNotNull(pluginsDirPath);
        LOG.debug("pluginsDirPath={}", pluginsDirPath);

    }


}
