package com.unlimited.demo.rest.spring.plugins;

import com.unlimited.demo.rest.spring.spi.CommercialPriceProvider;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.net.URL;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests for {@link CommercialPriceProviderFactoryImpl}
 *
 * @author Iulian Dumitru
 */
public class CommercialPriceProviderFactoryImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(CommercialPriceProviderFactoryImplTest.class);

    @Test
    public void testGetCommercialPriceProvider() throws Exception {

        URL url = getClass().getResource("/");
        assertNotNull("Unable to find root test classes dir!", url);

        String path = url.getFile();
        CommercialPriceProviderFactoryImpl factory = new CommercialPriceProviderFactoryImpl(path);

        CommercialPriceProvider priceProvider = factory.getCommercialPriceProvider("HBO");
        assertNotNull("Cannot find HBO price provider!", priceProvider);
        LOG.debug("priceProvider={}", priceProvider.getClass().getName());

        BigDecimal hboPrice = priceProvider.computeCommercialPrice(null, null, 0, 0);
        LOG.debug("hboPrice={}", hboPrice);

        priceProvider = factory.getCommercialPriceProvider("Discovery");
        assertNotNull("Cannot find Discovery price provider!", priceProvider);
        LOG.debug("priceProvider={}", priceProvider.getClass().getName());

        BigDecimal discoveryPrice = priceProvider.computeCommercialPrice(null, null, 0, 0);
        LOG.debug("discoveryPrice={}", discoveryPrice);

        priceProvider = factory.getCommercialPriceProvider("UNKNOWN");
        assertNull(priceProvider);

    }


}
