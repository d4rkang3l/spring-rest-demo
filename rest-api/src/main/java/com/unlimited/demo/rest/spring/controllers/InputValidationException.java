package com.unlimited.demo.rest.spring.controllers;

import com.unlimited.demo.rest.spring.commons.exception.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Input validation exception. Mapped to a HTTP 400 error status (bad request)
 *
 * @author Iulian Dumitru
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InputValidationException extends BusinessException {

    public InputValidationException(String message) {
        super(message);
    }

    public InputValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
