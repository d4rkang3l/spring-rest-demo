package com.unlimited.demo.rest.spring.controllers;

import com.unlimited.demo.rest.spring.commons.exception.ParsingException;
import com.unlimited.demo.rest.spring.service.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Base REST controller.
 * Defines exception handling.
 *
 * @author Iulian Dumitru
 */
public abstract class RESTController {

	private Logger LOG = LoggerFactory.getLogger(RESTController.class);

	/**
	 * Maps java exceptions to BAD_REQUEST (400) HTTP status code.
	 */
	@ExceptionHandler({
			InputValidationException.class,
			ParsingException.class,
			IllegalArgumentException.class,
			NumberFormatException.class,
			MissingServletRequestParameterException.class
	})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void mapToBadRequest(Exception e, HttpServletResponse response) throws IOException {

		LOG.error("Caught exception! Bad request: {}", e.getMessage());

		response.getWriter().write("{ status: 400, description: 'Bad request', message: '" + e.getMessage() + "' }");

	}

	/**
	 * Maps java exceptions to INTERNAL_SERVER_ERROR (500) HTTP status code.
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public void mapToInternalServerError(Exception e, HttpServletResponse response) throws IOException {

		LOG.error("Caught exception! Mapping to Internal server error:", e);

		response.getWriter().write("{ status: 500, description: 'Internal server error', message: 'An internal error occurred. Please try later.' }");

	}

	/**
	 * Maps java exceptions to NOT_FOUND (404) HTTP status code.
	 */
	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void mapToNotFound(Exception e, HttpServletResponse response) throws IOException {

		LOG.error("Caught exception! Mapping to Not found: {}", e.getMessage());

		response.getWriter().write("{ status: 404, description: 'Not found', message: '" + e.getMessage() + "' }");

	}


}
