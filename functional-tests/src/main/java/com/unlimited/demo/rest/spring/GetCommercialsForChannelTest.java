package com.unlimited.demo.rest.spring;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.CommonsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Test for getting commercials.
 */
public class GetCommercialsForChannelTest {

	private static final Logger LOG = LoggerFactory.getLogger(GetCommercialsForChannelTest.class);

	public static void main(String[] args) {

		try {

			HttpClient client = new HttpClient();
			UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("user", "user");
			client.getState().setCredentials(
					new AuthScope("localhost", 8080, AuthScope.ANY_REALM),
					credentials
			);
			CommonsClientHttpRequestFactory factory = new CommonsClientHttpRequestFactory(client);

			RestTemplate restTemplate = new RestTemplate(factory);
			String result = restTemplate.getForObject("http://localhost:8080/api/channels/HBO/commercials?from=2012.09.02.11.08.00&to=2012.09.02.15.00.00&duration=10&occurrencesNr=2&pageNr=1&pageSize=10", String.class);

			LOG.debug("result={}", result);

		} catch (RestClientException e) {

			LOG.error("RestClientException:", e);

		}

	}

}
