package com.unlimited.demo.rest.spring;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.CommonsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Test plugin upload.
 *
 * @author Iulian Dumitru
 */
public class UploadPluginJarTest {

	private static final Logger LOG = LoggerFactory.getLogger(UploadPluginJarTest.class);

	public static void main(String[] args) {

		try {
			LOG.debug("Testing plugin upload ...");

			HttpClient client = new HttpClient();
			UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("admin", "admin");
			client.getState().setCredentials(
					new AuthScope("localhost", 8080, AuthScope.ANY_REALM),
					credentials
			);
			CommonsClientHttpRequestFactory factory = new CommonsClientHttpRequestFactory(client);

			RestTemplate restTemplate = new RestTemplate(factory);


			MultiValueMap<String, Object> form = new LinkedMultiValueMap<String, Object>();
//		ClassPathResource resource = new ClassPathResource("/discovery-service-1.0.jar");
			ClassPathResource resource = new ClassPathResource("/hbo-service-1.0.jar");
			form.add("file", resource);

			restTemplate.postForLocation("http://localhost:8080/api/plugins", form);

			LOG.debug("Testing plugin upload ... done.");

		} catch (RestClientException e) {

			LOG.error("RestClientException: ", e);

		}

	}

}
