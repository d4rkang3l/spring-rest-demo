package com.unlimited.demo.rest.spring.service;

import com.unlimited.demo.rest.domain.Commercial;
import com.unlimited.demo.rest.spring.repository.CommercialRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link CommercialServiceImpl}
 *
 * @author Iulian Dumitru
 */
public class CommercialServiceImplTest {

    private CommercialServiceImpl service;
    private CommercialRepository repository;

    @Before
    public void setUp() throws Exception {
        repository = mock(CommercialRepository.class);
        service = new CommercialServiceImpl(repository);
    }

    @Test
    public void testGetCommercials() throws Exception {

        //mock
        List<Commercial> commercials = new ArrayList<Commercial>();
        commercials.add(new Commercial());
        when(repository.getCommercials(anyString(), any(Date.class), any(Date.class), any(Long.class), any(Long.class), any(PageRequest.class)))
                .thenReturn(commercials);

        commercials = service.getCommercials("HBO", new Date(), new Date(), 10L, 10L, null);

        //check that the repository was called
        verify(repository).getCommercials(anyString(), any(Date.class), any(Date.class), any(Long.class), any(Long.class), any(PageRequest.class));

        assertNotNull(commercials);

    }
    @Test(expected = CommercialNotFoundException.class)
    public void testGetCommercialsNotFound() throws Exception {

        //mock
        List<Commercial> commercials = new ArrayList<Commercial>();
        when(repository.getCommercials(anyString(), any(Date.class), any(Date.class), any(Long.class), any(Long.class), any(PageRequest.class)))
                .thenReturn(commercials);

        service.getCommercials("HBO", new Date(), new Date(), 10L, 10L, null);
    }

}
