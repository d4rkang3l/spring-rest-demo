package com.unlimited.demo.rest.spring.repository;

import com.unlimited.demo.rest.domain.Commercial;
import com.unlimited.demo.rest.spring.plugins.CommercialPriceProviderFactory;
import com.unlimited.demo.rest.spring.spi.CommercialPriceProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link CommercialRepository}
 *
 * @author Iulian Dumitru
 */
public class CommercialRepositoryImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(CommercialRepositoryImplTest.class);

    public static final CommercialPriceProvider HBO_PROVIDER = new CommercialPriceProvider() {
        @Override
        public String getChannelName() {
            return "HBO";
        }

        @Override
        public BigDecimal computeCommercialPrice(Date from, Date to, long duration, long occurrencesNr) {
            return new BigDecimal("44.88");
        }
    };

    private CommercialRepository repository;

    @Before
    public void setUp() throws Exception {

        CommercialPriceProviderFactory factory = Mockito.mock(CommercialPriceProviderFactory.class);
        when(factory.getCommercialPriceProvider("HBO")).thenReturn(HBO_PROVIDER);

        repository = new CommercialRepositoryImpl(factory);

    }

    @Test
    public void testGetCommercials() throws Exception {

        List<Commercial> commercials = repository.getCommercials("HBO", new Date(), new Date(), 10, 10, null);
        assertNotNull(commercials);

        for (Commercial commercial : commercials) {
            LOG.debug("commercial={}", commercial);
        }

    }

    @Test
    public void testGetCommercialsNoProvider() throws Exception {

        List<Commercial> commercials = repository.getCommercials("Discovery", new Date(), new Date(), 10, 10, null);
        assertNull(commercials);

    }

}
