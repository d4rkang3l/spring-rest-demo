package com.unlimited.demo.rest.spring.repository;

import com.unlimited.demo.rest.domain.Commercial;
import com.unlimited.demo.rest.spring.plugins.CommercialPriceProviderFactory;
import com.unlimited.demo.rest.spring.service.PageRequest;
import com.unlimited.demo.rest.spring.spi.CommercialPriceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Default implementation for {@link CommercialRepository}
 *
 * @author Iulian Dumitru
 */
@Component
public class CommercialRepositoryImpl implements CommercialRepository {

    private static final Logger LOG = LoggerFactory.getLogger(CommercialRepositoryImpl.class);

    private CommercialPriceProviderFactory factory;

    @Autowired
    public CommercialRepositoryImpl(CommercialPriceProviderFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Commercial> getCommercials(String channel, Date from, Date to, long duration, long occurrencesNr, PageRequest pageRequest) {

        Object[] params = {channel, from, to, duration, occurrencesNr, pageRequest};
        LOG.debug("CommercialRepositoryImpl.getCommercials(channel={}, from={}, to={}, duration={}, occurrencesNr={}, pageRequest={}",
                params);

        CommercialPriceProvider priceProvider = factory.getCommercialPriceProvider(channel);
        if (priceProvider == null) {
            LOG.warn("Unable to find price provider for channel {}!", channel);
            return null;
        }

        BigDecimal price = priceProvider.computeCommercialPrice(from, to, duration, occurrencesNr);

        //dummy data (in a real app this data will be retrieved from a database or something similar)
        List<Commercial> commercials = new ArrayList<Commercial>();

        Commercial c1 = new Commercial();
        c1.setChannel(channel);
        c1.setDuration(20);
        c1.setPrice(price);
        c1.setName("Cool commercial");
        c1.setDescription("A very cool commercial");
        commercials.add(c1);

        Commercial c2 = new Commercial();
        c2.setChannel(channel);
        c2.setDuration(30);
        c2.setPrice(price);
        c2.setName("HD commercial");
        c2.setDescription("Something interesting");
        commercials.add(c2);

        return commercials;
    }

    @Override
    public List<String> getChannels() {

        LOG.debug("CommercialRepositoryImpl.getChannels() ...");

        //in a functional world this would be very simple to implement :)
        List<CommercialPriceProvider> list = factory.getProviders();
        List<String> channels = new ArrayList<String>();
        for (CommercialPriceProvider commercialPriceProvider : list) {
            channels.add(commercialPriceProvider.getChannelName());
        }

        LOG.debug("DONE CommercialRepositoryImpl.getChannels().");

        return channels;
    }

}
