package com.unlimited.demo.rest.spring.plugins;

import com.unlimited.demo.rest.spring.spi.CommercialPriceProvider;

import java.util.List;

/**
 * Factory of {@link CommercialPriceProvider}
 *
 * @author Iulian Dumitru
 */
public interface CommercialPriceProviderFactory {

    /**
     * Get a {@link com.unlimited.demo.rest.spring.spi.CommercialPriceProvider} by channel
     *
     * @param channel channel
     * @return a provider by channel
     */
    CommercialPriceProvider getCommercialPriceProvider(String channel);

    /**
     * Return available providers.
     *
     * @return providers
     */
    List<CommercialPriceProvider> getProviders();

}
