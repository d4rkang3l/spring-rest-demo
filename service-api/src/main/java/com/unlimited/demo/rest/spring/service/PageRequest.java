package com.unlimited.demo.rest.spring.service;

/**
 * Page request (use this to detail pagination process).
 *
 * @author Iulian Dumitru
 */
public class PageRequest {

    private final int pageNumber;
    private final int pageSize;

    public PageRequest(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PageRequest)) {
            return false;
        }

        PageRequest that = (PageRequest) o;

        if (pageNumber != that.pageNumber) {
            return false;
        }
        if (pageSize != that.pageSize) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = pageNumber;
        result = 31 * result + pageSize;
        return result;
    }

    @Override
    public String toString() {
        return "PageRequest{" +
                "pageNumber=" + pageNumber +
                ", pageSize=" + pageSize +
                '}';
    }

}
