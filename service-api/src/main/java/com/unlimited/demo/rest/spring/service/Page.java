package com.unlimited.demo.rest.spring.service;

/**
 * @author Iulian Dumitru
 */
public class Page<T> {

	private int pageNumber;
	private int pageSize;
	private T content;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Page)) {
			return false;
		}

		Page page = (Page) o;

		if (pageNumber != page.pageNumber) {
			return false;
		}
		if (pageSize != page.pageSize) {
			return false;
		}
		if (content != null ? !content.equals(page.content) : page.content != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = pageNumber;
		result = 31 * result + pageSize;
		result = 31 * result + (content != null ? content.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Page{" +
				"pageNumber=" + pageNumber +
				", pageSize=" + pageSize +
				'}';
	}

}
