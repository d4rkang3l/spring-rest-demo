package com.unlimited.demo.rest.spring.commons.date;

import com.google.common.base.Strings;
import com.unlimited.demo.rest.spring.commons.exception.ParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Date utilities.
 *
 * @author Iulian Dumitru
 */
public final class DateUtils {

    private static final Logger LOG = LoggerFactory.getLogger(DateUtils.class);

    //Prevent instantiation
    private DateUtils() {
    }

    /**
     * Create a Date object with a specific date format
     *
     * @param s      input string
     * @param format date format
     * @return a Date object with a specific date format
     * @throws ParsingException cannot parse input string
     */
    public static Date getDateWithFormat(String s, String format) {

        checkArgument(!Strings.isNullOrEmpty(s), "Param 's' is null or empty!");
        checkArgument(!Strings.isNullOrEmpty(format), "Param 'format' is null or empty!");

        DateFormat formatter = new SimpleDateFormat(format);
        try {
            return formatter.parse(s);
        } catch (ParseException e) {
            String message = "Cannot parse string '" + s + "' for date format '" + format + "'! Check java.text.SimpleDateFormat javadoc!";
            LOG.error(message, e);
            throw new ParsingException(message);
        }
    }


}
