package com.unlimited.demo.rest.spring.commons.exception;

/**
 * System exception.
 * <p/>
 * An exception from which the system cannot recover (network connection, DB failure, programming error etc).
 *
 * @author Iulian Dumitru
 */
public abstract class SystemException extends RuntimeException {

    public SystemException() {
    }

    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }


}
